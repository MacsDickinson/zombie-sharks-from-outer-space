﻿$(function () {
    document.onselectstart = function () { return false; };
    //$('#html').css('background-image', 'url(../Images/spacebg' + Math.ceil(Math.random() * 7) + '.jpg)');
    $('#canvas-container').css('background-image', 'url(../Images/spacebg' + Math.ceil(Math.random() * 7) + '.jpg)');
    setup();
});
var laser, menu, game, canvas, lasercanvas, context, lasercontext, frameRefereshID, laserFireID, laserBeepID = null;
var laserStrength, baseDefenseHealth, prideSize, secondsRemaining = 0;
var audiocontext = new webkitAudioContext();
var sharkassets = ['evilshark1.png',
			  'evilshark2.png',
			  'evilshark3.png',
			  'evilshark4.png',
			  'evilshark5.png',
			  'evilshark4.png',
			  'evilshark3.png',
			  'evilshark2.png'];
var lionassets = ['laserlion_closed.png'];
var cursorAssests = ['off_crosshair.png'];
var frames = [];
var height1 = 0;
var height2 = 0;

var setup = function () {
    game = new GameClass();

    var container = document.getElementById('canvas-container');
    canvas = document.createElement('canvas');
    context = canvas.getContext('2d');
    canvas.width = 900;
    canvas.height = 700;
    container.appendChild(canvas);

    lasercanvas = document.createElement('canvas');
    lasercontext = lasercanvas.getContext('2d');
    lasercanvas.id = "laserCanvas";
    lasercanvas.width = 900;
    lasercanvas.height = 700;

    container.appendChild(canvas);
    container.appendChild(lasercanvas);

    game.loadSprite("Images/mainsprite.png");

    laser = new LaserClass();
    laser.load();
    menu = new MenuClass();

    lasercanvas.addEventListener("mousedown", fireLaser, false);
    lasercanvas.addEventListener("mouseup", stopLaser, false);
    lasercanvas.addEventListener("mousemove", moveLaser, false);

    frameRefereshID = setInterval(game.frameRefereshInterval, 1000 / 30);
    laserFireID = setInterval(laser.FireInterval, 1000 / 30);
    laserBeepID = setInterval(laser.BeepInterval, 200);

    laserStrength = 371;
    baseDefenseHealth = 1000;
    prideSize = 1;
    menu.main();
};
var moveLaser = function (event) {
    laser.move(event.x, event.y);
};
var stopLaser = function () {
    laser.stop();
};
var fireLaser = function () {
    laser.fire();
};

GameClass = Class.extend({
    active: false,
    level: 0,
    score: 0,
    secondsRemaining: 0,
    enemyInterval: 0,
    enemyIntervalID: 0,
    levelTimerID: 0,
    defensesHealth: 0,
    nextLevel: function () {
        this.level++;
        this.startLevel();
    },
    startLevel: function () {
        this.active = true;
        this.enemyInterval = 10000 / this.level;
        this.secondsRemaining = 20 * this.level;
        this.addLion();
        this.enemyIntervalID = setInterval(this.addSharkInterval, this.enemyInterval);
        this.defensesHealth = baseDefenseHealth;
        $('#game-score').text(this.score);
        $('#game-level').text(this.level);
        $('#game-defense-health').text(this.defensesHealth);
        this.levelTimerID = setInterval(this.levelTimerInterval, 1000);
    },
    stopLevel: function () {
        window.clearInterval(this.enemyIntervalID);
        window.clearInterval(this.levelTimerID);
        this.clearCanvas();
        this.active = false;
    },
    levelComplete: function () {
        this.stopLevel();
        menu.levelComplete();
    },
    gameOver: function () {
        this.stopLevel();
        menu.gameover();
    },
    addLion: function () {
        var lion = new FrameImageClass();
        lion.load(lionassets, 100, 200, 0, 'static', 450, 620);
        frames.push(lion);
    },
    updateScore: function (amount) {
        game.score += amount;
        $('#game-score').text(game.score);
    },
    updateDefenseHealth: function (damage) {
        game.defensesHealth -= damage;
        $('#game-defense-health').text(game.defensesHealth);
        if (game.defensesHealth <= 0) {
            game.gameOver();
        }
    },
    clearCanvas: function () {
        frames = [];
        context.clearRect(0, 0, canvas.width, canvas.height);
        lasercontext.clearRect(0, 0, canvas.width, canvas.height);
    },
    removeCompleteFrames: function (frame) {
        switch (frame.direction) {
            case 'up':
                if (frame.positiony < -(frame.height * 0.5)) {
                    frames.erase(frame);
                }
                break;
            case 'down':
                if (frame.positiony > canvas.height + (frame.height * 0.5)) {
                    this.updateDefenseHealth(frame.health);
                    frames.erase(frame);
                }
                break;
            default:
                break;
        }
    },
    loadSprite: function (imgName) {
        $.ajax({
            url: "Home/ImageJSON",
            data: {
                'imgName': imgName
            },
            success: function (imgJSON) {

                var sprirsheet = new SpriteSheetClass();
                sprirsheet.load(imgName);
                sprirsheet.parseAtlasDefinition(imgJSON);
            }
        });
    },
    countdown: function () {
        $('#game-countdown').text(this.secondsRemaining);
        if (this.secondsRemaining === 0) {
            this.levelComplete();
        }
        this.secondsRemaining--;
    },
    levelTimerInterval: function () {
        game.countdown();
    },
    addSharkInterval: function () {
        var shark = new ZombieSharkClass();
        var height = Math.ceil(Math.random() * 200) + 100;
        var width = Math.ceil(Math.random() * 150) + 50;
        var speed = Math.ceil(Math.random() * 3);
        var posX = Math.ceil(Math.random() * 800) + 50;
        shark.load(sharkassets, height, width, speed, 'down', posX, -(height * 0.5));
        shark.health = height * width;
        frames.push(shark);
    },
    frameRefereshInterval: function () {
        context.clearRect(0, 0, canvas.width, canvas.height);
        for (var i = 0; i < frames.length; i++) {
            var frame = frames[i];
            frame.draw();
            game.removeCompleteFrames(frame);
        }
        if (laser.cursor) laser.cursor.draw();
    }
});

