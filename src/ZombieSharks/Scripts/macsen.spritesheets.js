

// We keep a global dictionary of loaded sprite-sheets,
// which are each an instance of our SpriteSheetClass
// below.
//
// This dictionary is indexed by the URL path that the
// atlas is located at. For example, calling:
//
// gSpriteSheets['grits_effects.png']
//
// would return the SpriteSheetClass object associated
// to that URL, assuming that it exists.
var gSpriteSheets = {};

//-----------------------------------------
SpriteSheetClass = Class.extend({
    // We store in the SpriteSheetClass:
    //
    // The Image object that we created for our
    // atlas.
    img: null,

    // The URL path that we grabbed our atlas
    // from.
    url: "",

    // An array of all the sprites in our atlas.
    sprites: [],

    //-----------------------------------------
    init: function() {
    },

    //-----------------------------------------
    // Load the atlas at the path 'imgName' into
    // memory. This is similar to how we've
    // loaded images in previous units.
    load: function(imgName) {
        this.url = imgName;

        var img = new Image();
        img.src = imgName;

        this.img = img;

        gSpriteSheets[imgName] = this;
    },

    defSprite: function(name, x, y, w, h, cx, cy) {
        var spt = {
            "id": name,
            "x": x,
            "y": y,
            "w": w,
            "h": h,
            "cx": cx === null ? 0 : cx,
            "cy": cy === null ? 0 : cy
        };

        this.sprites.push(spt);
    },
    getJSONResponse: function(response) {
        parseAtlasDefinition(response.data);
    },
    parseAtlasDefinition: function (atlasJSON) {

        var parsed = JSON.parse(atlasJSON);
		for(var key in parsed.frames) {
			var sprite = parsed.frames[key];
		    if (sprite.frame) {
		        var cx = -sprite.frame.w * 0.5;
		        var cy = -sprite.frame.h * 0.5;

		        if (sprite.trimmed) {
		            cx = sprite.spriteSourceSize.x - (sprite.sourceSize.w / 2);
		            cy = sprite.spriteSourceSize.y - (sprite.sourceSize.h / 2);
		        }

		        this.defSprite(sprite.filename, sprite.frame.x, sprite.frame.y, sprite.frame.w, sprite.frame.h, cx, cy);
		    }
		}
	},

	getStats: function (name) {
		for(var i = 0; i < this.sprites.length; i++) {
            if(this.sprites[i].id === name) {
                return this.sprites[i];
            }

		}
		return null;
	}

});

function drawSprite(spritename, posX, posY) {
    for (var sheetName in gSpriteSheets) {
        var sheet = gSpriteSheets[sheetName];
        var sprite = sheet.getStats(spritename);

        if (sprite === null) {
            continue;
        }
        __drawSpriteInternal(sprite, sheet, posX, posY, sprite.w, sprite.h);
        return;
    }
}

function drawSpriteExact(spritename, posX, posY, height, width) {
    for (var sheetName in gSpriteSheets) {
        var sheet = gSpriteSheets[sheetName];
        var sprite = sheet.getStats(spritename);

        if (sprite === null) {
            continue;
        }
        __drawSpriteInternal(sprite, sheet, posX, posY, height, width);
        return;
    }
}

function __drawSpriteInternal(spt, sheet, posX, posY, height, width) {
    if (spt === null || sheet === null) {
        return;
    }

    var hlf = {
        x: spt.cx,
        y: spt.cy
    };

    context.drawImage(sheet.img, spt.x, spt.y, spt.w, spt.h, posX + hlf.x, posY + hlf.y, width, height);
}