﻿FrameImageClass = Class.extend({
    frames: [],
    frame: 0,
    height: null,
    width: null,
    speed: null,
    direction: null,
    positionx: 0,
    positiony: 0,

    init: function() {
    },
    load: function (frames, height, width, speed, direction, positionx, positiony) {
        this.frames = frames;
        this.height = height;
        this.width = width;
        this.speed = speed;
        this.direction = direction;
        this.positionx = positionx;
        this.positiony = positiony;
    },
    draw: function () {
        if (this.height === null || this.width === null) {
            drawSprite(this.frames[this.frame], this.positionx, this.positiony);
        } else {
            drawSpriteExact(this.frames[this.frame], this.positionx, this.positiony, this.height, this.width);
        }
        this.move();
        this.animate();
    },
    move: function () {
        switch (this.direction) {
            case 'up':
                this.positiony -= this.speed;
                break;
            case 'down':
                this.positiony += this.speed;
                break;
            default:
                break;
        }
    },
    animate: function() {
        this.frame = (this.frame + 1) % this.frames.length;
    }
});