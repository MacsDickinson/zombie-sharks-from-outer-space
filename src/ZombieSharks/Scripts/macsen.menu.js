﻿MenuClass = Class.extend({
    canvas: null,
    context: null,
    container: null,
    init: function () {
        container = document.getElementById('canvas-container');

        var mainmenuItems = [
            { name: "New Game", callback: this.newgame },
            { name: "Select Level", callback: this.commingsoon },
            { name: "View Highscores", callback: this.commingsoon },
            { name: "Open Store", callback: this.commingsoon }
        ];
        this.addMenu("Main Menu", mainmenuItems);
        var gameovermenuItems = [
            { name: "Replay Level", callback: this.replay },
            { name: "Submit Score", callback: this.commingsoon },
            { name: "Main Menu", callback: this.switchToMainMenu }
        ];
        this.addMenu("Game Over", gameovermenuItems);
        var levelcompletemenuitems = [
            {name: "Next Level", callback: this.nextLevel },
            { name: "Replay Level", callback: this.replay },
            { name: "Submit Score", callback: this.commingsoon },
            { name: "Main Menu", callback: this.switchToMainMenu }
        ];
        this.addMenu("Level Complete", levelcompletemenuitems);
        $("a[rel*=leanModal]").leanModal({ overlay: 0.6, closeButton: ".modal-close" });
    },
    load: function () {
    },
    main: function() {
        this.show("main-menu");
    },
    gameover: function () {
        this.show("game-over");
    },
    levelComplete: function () {
        this.show("level-complete");
    },
    show: function(menuName) {
        $('#game-'+menuName+'-trigger').click();
    },
    addMenu: function (name, items) {
        var $menuDiv = $('<div/>', {
            id: "game-" + name.replace(" ", "-").toLowerCase(),
            class: "menu-container"
        }).appendTo('#game-container');
        $('<a/>', {
            text: "Close",
            class: "modal-close"
        }).appendTo($menuDiv);
        var $fieldset = $('<fieldset />').appendTo($menuDiv);;
        $('<legend />', {
            text: name
        }).appendTo($fieldset);
        var $ul = $('<ul />').appendTo($fieldset);;
        for (var i = 0; i < items.length; i++) {
            var $li = $('<li />').appendTo($ul);;
            $('<a />', {
                id: "game-" + items[i].name.replace(" ", "-").toLowerCase(),
                text: items[i].name,
                href: '#',
                click: items[i].callback
            }).appendTo($li);
        }
        $('<a/>', {
            id: "game-" + name.replace(" ", "-").toLowerCase() + '-trigger',
            rel: 'leanModal',
            href: "#game-" + name.replace(" ", "-").toLowerCase(),
            class: 'modal-trigger'
        }).appendTo('.game-footer');
    },
    commingsoon: function() {
        alert('Comming Soon');
    },
    switchToMainMenu: function () {
        $('.modal-close').click();
        $('#game-main-menu-trigger').click();
    },
    replay: function() {
        game.score = 0;
        game.startLevel();
        $('.modal-close').click();
    },
    newgame: function() {
        $('#game-laser-strength').text(laserStrength);
        $('#game-pride').text(prideSize);
        game.level = 1;
        game.startLevel();
        $('.modal-close').click();
    },
    nextLevel: function() {
        game.nextLevel();
        $('.modal-close').click();
    }
});