﻿LaserClass = Class.extend({
    firing: false,
    hit: false,
    laserBeep: null,
    laserHit: null,
    cursor: new FrameImageClass(),
    lionroar: null,
    init: function() {
    },
    load: function() {
        var soundRequest = new XMLHttpRequest();
        soundRequest.open("GET", "../Assets/laser_sin.mp3", true);
        soundRequest.responseType = "arraybuffer";
        soundRequest.onload = function () {
            laserBeep = soundRequest.response;
        };
        soundRequest.send();
        var soundRequest2 = new XMLHttpRequest();
        soundRequest2.open("GET", "../Assets/laser_saw.mp3", true);
        soundRequest2.responseType = "arraybuffer";
        soundRequest2.onload = function () {
            laserHit = soundRequest2.response;
        };
        soundRequest2.send();
        var soundRequest3 = new XMLHttpRequest();
        soundRequest3.open("GET", "../Assets/lionroar.mp3", true);
        soundRequest3.responseType = "arraybuffer";
        soundRequest3.onload = function () {
            lionroar = soundRequest3.response;
        };
        soundRequest3.send();
    },
    fire: function() {
        if (game.active) {
            this.firing = true;
            cursorAssests[0] = 'on_crosshair.png';
            lionassets[0] = 'laserlion_open.png';
            this.draw('lime', 340 + (laserStrength / 200), 645, this.cursor.positionx + 5, this.cursor.positiony + 5);
            this.draw('blue', 340, 645, this.cursor.positionx + 5, this.cursor.positiony + 5);
            this.draw('red', 340 - (laserStrength / 200), 645, this.cursor.positionx + 5, this.cursor.positiony + 5);
        }
    },
    stop: function() {
        this.firing = false;
        lasercontext.clearRect(0, 0, lasercanvas.width, lasercanvas.height);
        cursorAssests[0] = 'off_crosshair.png';
        lionassets[0] = 'laserlion_closed.png';
    },
    move: function(x, y) {
        x -= lasercanvas.offsetLeft;
        y -= lasercanvas.offsetTop;
        this.drawCursor(x, y);
        if (this.firing) {
            this.stop();
            this.fire();
        }
    },
    draw: function (color, startx, starty, endx, endy) {
        lasercontext.beginPath();
        lasercontext.lineWidth = laserStrength / 100;
        lasercontext.strokeStyle = color;
        lasercontext.moveTo(startx, starty);
        lasercontext.lineTo(endx, endy);
        lasercontext.stroke();
    },
    drawCursor: function (x, y) {
        this.cursor = new FrameImageClass();
        this.cursor.load(cursorAssests, 20, 20, 0, 'static', x - 5, y - 5);
    },
    makeNoise: function (noise) {
        if (audiocontext) {
            var mainNode = audiocontext.createGainNode(0);
            mainNode.connect(audiocontext.destination);
            var clip = audiocontext.createBufferSource();
            audiocontext.decodeAudioData(noise, function(buffer) {
                clip.buffer = buffer;
                clip.gain.value = 1.0;
                clip.connect(mainNode);
                clip.noteOn(0);
            }, function() {
            });
        }
    },
    hurtSharks: function (laserx, lasery) {
        var hitAny = false;
        for (var i = 0; i < frames.length; i++) {
            var frame = frames[i];
            if (frame.positionx - (frame.width * 0.5) < laserx & laserx < frame.positionx + (frame.width * 0.5)) {
                if (frame.positiony - (frame.height * 0.5) < lasery & lasery < frame.positiony + (frame.height * 0.5)) {
                    hitAny = true;
                    frame.health -= laserStrength;
                    game.updateScore(laserStrength);
                    if (frame.health < 0) {
                        game.updateScore(frame.height * frame.width);
                        frames.erase(frame);
                    }
                }
            }
        }
        this.hit = hitAny;
    },
    FireInterval: function () {
        if (game.active && laser.firing) {
            laser.hurtSharks(laser.cursor.positionx, laser.cursor.positiony);
        }
    },
    BeepInterval: function () {
        if (game.active && laser.firing) {
            if (laser.hit) {
                laser.makeNoise(laserHit);
            } else {
                laser.makeNoise(laserBeep);
            }
        }
    }
});