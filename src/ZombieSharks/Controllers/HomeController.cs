﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.Mvc;

namespace ZombieSharks.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        public string ImageJSON(string imgName)
        {
            string jsonName = imgName.Replace("png", "json");
            string filePath = Server.MapPath(Url.Content("~/"+jsonName));
            StringBuilder sb = new StringBuilder();
            using (StreamReader sr = new StreamReader(filePath))
            {
                sb.Append(sr.ReadToEnd());
            }
            return sb.ToString();
        }
    }
}
